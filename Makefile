TARGET = target
SOURCE_FOLDER = src
SOURCE_FILES := $(sort $(SOURCE_FOLDER)/*.md)

all: 
	mkdir -p $(TARGET)
	pandoc --standalone $(SOURCE_FILES) -o $(TARGET)/notes_ift_4102.pdf

clean:
	rm -rf $(TARGET)

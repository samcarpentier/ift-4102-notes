# IFT-4102 notes [![pipeline status](https://gitlab.com/samcarpentier/ift-4102-notes/badges/master/pipeline.svg)](https://gitlab.com/samcarpentier/ift-4102-notes/commits/master)

To download latest generated PDF, click [here](https://gitlab.com/samcarpentier/ift-4102-notes/-/jobs/artifacts/master/raw/notes_ift_4102.pdf?job=create_pdf_artifact)

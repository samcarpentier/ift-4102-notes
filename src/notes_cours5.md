# Recherche adversariale

* Adversaire en face de soi (plusieurs agents)
* Comment l'un choisit ses actions en fonction de ce que fait l'autre

## Introduction

### Rappel sur A*

* Fonction de transition (trouver successeurs)
* Notion du but (config finale)
* Remodelons le A* en jeu adversarial (pas simple...):
    * Définir état pour 2 agents
    * Action pour passer d'un état à l'autre est le résultat combiné des actions des 2 agents
    * Changement d'environnement

* Complexité jeux adversariaux:
    * Plusieurs acteurs modifient environnement
    * Coups "imprévisibles" de l'adversaire
    * plus on a d'agent, plus c'est complexe
    * Temps de réaction limité

### Relation entre les joueurs

* Coopératifs: veulent atteindre le même but
* Adversaires: gain de l'un = perte de l'autre (somme nulle)

* Hypothèse de ce cours:
    * Jeux à 2 adversaires
    * Jeux à tour de rôle
    * Jeux à somme nulle
    * Jeux déterministes (pas de hasard)
    * Utilisation de systèmes multiagents (SMA)

* Max vs Min:
    * Max joue en premier (nous)
    * Min est notre adversaire
    * Distribution d'une récompense en fonction du résultat d'une partie
    * Récompense = résultat d'un pari
    * Résultats possibles: {(1,-1), (-1,1), (0,0)}

### Arbre de recherche

* Plus complexe que d'habitude: présence d'un adversaire
* Jeu adversarial = recherche dans un arbre

* Neud initial
* Fonction de transition produit des paires (action, successeur):
    * Action légale
    * Neoud = résultat de l'action sur l'état courant

* Est-ce que l'agent a de l'information sur l'autre? **NON** Obsevation de l'état courant après le coup de l'autre uniquement.

## Algorithme _Minimax_ (intro)

* Un agent minimise, l'autre maximise
* Valeur 0 = match nul
* Valeur 1 = _max_ a gagné
* Valeur -1 = _min_ a gagné
* Ces valeurs sont appelées **utilité**

### Détails d'implémentation

* Idée: à chaque tour, choisir l'action menant à la plus grande valeur _minimax_
* On joue contre un joueur optimal (min ou max)

* Bas vers le haut: agent _min_ joue coup avec la plus petite utilité (à partir du haut de l'arbre)
* Haut vers le bas: agent _max_ joue coup avec la plus grande utilité (à partir du bas de l'arbre)

* Programmation récusrive des valeurs _minimax_ sur l'arbre

### Propriétés _Minimax_

* Complétude: oui si arbre fini
* Optimal: Oui
* Complexité temps: O(b^m):
    * _b_: nombre maximum d'actions légales à chaque étape
    * _m_: nombre maximum de coups dans un jeu (profondeur max de l'arbre)
* Complexité en espace: O(bm) -> Effectue une recherche en profondeur
* Échecs: b = 35, m = 100 pour un jeu "raisonnable" -> O(35^100) **pas réaliste de trouver une solution en temps réel**

## Comment peut-on accélérer la recherche?

* Deux approches:
    1. Élagage alpha-beta (alpha-beta pruning)
        * Maintient l'exactitude de la solution
        * Retirer chemins inutiles de l'arbre (ne pas les explorer)
    2. Couper la recherche et remplacer l'utilité par une heuristique
        * Recherche la plus profonde possible en fonction du temps
        * Tenter de prédire le résultat de la partie si l'on arrive pas à la fin

## Élagage alpha-beta

* Joueurs max-min
* Certaines feuilles de l'arbre d'utilité quelconque peuvent être omises
* On a donc coupé un peu sur la profondeur à certains endroits
* Aller voir exemple sur Youtube

* Algo tire son nom des paramètres qui décrivent les bornes des valeurs d'utilités
* Valeurs enregistrées durant le parcours:
    * _alpha_: valeur du meilleur choix pour _max_ trouvé jusqu'à présent
    * _beta_: valeur du meilleur choix pour _min_ trouvé jusqu'à présent
* Élagage d'un côté ou de l'autre de l'arbre selon qu'on soit _max_ ou _min_

### Condition pour couper dans un noeud _min_

* Si on est dans un **noeud _min_** et que sa valeur _v_ devient inférieure à _alpha_, on arrête la recherche
* `si v > _alpha_: chop!`

### Propriétés d'alpha-beta

* L'élagage n'affecte pas le résultat final du _minimax_
* Pire cas: b^m noeuds
* Meilleur cas: O(b^(m/2)) dans le cas d'un ordonnancement parfait

## Désicion en temps réel (estimation)

* Pas le temps d'explorer tout l'arbre
* Limite la profondeur qu'on explore
* Estime la solution quand on manque de temps
* Solution optimale non garantie (estimation)

### Fonction d'évaluation

* Exemple jeu d'échec:
    * Somme linéaire pondérée: `Eval(s) = w1f1(s) + w2f2(s) + ... + wnfn(s)`

* Exemple tic-tac-toe:
    * Supposons que _max_ joue les "X":
        * `Eval(s) = (nb lignes, colonnes et diagonales dispo pour _max_) - (nb lignes, colonnes et diagonales dispo pour _min_)`

## Généralisation au actions aléatoires

* Présence d'un "3e" joueur: l'environnement
    * Jeux où on lance un dé pour déterminer la prochaine action
    * Actions des fantômes dans Pacman

* On ajoute des noeuds de chance en plus des noeuds _max_ et _min_
* Utilité du noeud de change est l'espérance (utilité espérée)
* Algorithme _Expectimax_

## Algorithme _Expectimax_

* On multiplie la probabilité par la valeur du _minimax_

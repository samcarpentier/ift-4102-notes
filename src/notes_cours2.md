# Agents intelligents

## Qu'est-ce que c'est?

* Un agent intelligent un entité qui:
    * **perçoit** son environnement à l’aide de ses **capteurs**
    * **agit** sur son environnement à l’aide de ses **effecteurs**

### Agent vs Objet

* Agent:
    * Entité autonome interagissant avec son environnement
    * Degré d'abstraction plus élevé qu'un objet
    * Généralement constitué de plusieurs objets
* Objet:
    * Entité passive ayant un état et sur lequel on peut effectuer des opérations

* Programmation orientée-agent:
    * Paradigme mettant de l'avant **l'autonomie et les interactions**

## Agent rationnel

* Exécute l'action qui **maximise sa performance** en fonction de sa perception et de ses connaissances

### Mesure de performance

* Externe
* Fixée par le concepteur
* Propre à la tâche

* Choix de la mesure: il faut faire attention!
    * Quantité de saleté ramassé en 8h (pour un aspirateur)
    * La saleté pourrait être redéposée plus loin!
* Meilleure option: récompenser l'agent pour un placher propre
    * 1 point pour chaque carré propre par intervalle de temps
    * Pénalité pour l'électricité consommée
    * Va inciter l'agent à maximiser sa performance

| Rationnalisme | =/= Rationnalisme |
| --- | --- |
| Exploration | Omniscience |
| Apprentissage | Clairvoyance |
| Autonomie | Succès |

## Environnement de la tâche

* Première étape quand on conçoit un agent: spécifier l'environnement
* 4 éléments (PEAS) :
    * Mesure de la performance (**P**erformance measure)
    * Environnement (**E**nvironment)
    * Effecteurs (**A**ctuators)
    * Capteurs (**S**ensors)

### Exemple conduite automatique taxi

* Type d'agent: chauffeur de taxi

    * PEAS:
    * Mesure de performance: sécurité, vitesse, légalité, confort, profit
    * Environnement: routes, autres véhicules, piétons, clients
    * Effecteurs: volant, accélérateur, frein, clignotant, kalxon
    * Capteurs: caméras, GPS, sonar, odomètre, speedomètre

### Propriétés de l'environnement

* Observable vs partiellement observable:
    * accès à l'état complet de l'environnement à tout instant
* Déterministe vs stochastique:
    * prochain état complètement déterminé par l'état courant et l'action de l'agent
* Épisodique vs séquentiel:
    * Épisode = séquence perception-action
    * Prochain épisode indépendant des épisodes précédents
* Statique vs dynamique:
    * Environnement change-t-il avec le temps
* Discret vs continu
    * État de l'environnement
    * Gestion du temps
    * Perceptions/actions de l'agent
* Un agent vs multiagent:
    * Un ou plusieurs agents qui interagissent dans le même environnement

* Solution la plus difficile:
    * Partiellement observable
    * Stochastique
    * Séquentiel
    * Dynamique
    * Continu
    * Multiagent
* Exemple: conduite intelligente d'un taxi
* Voir tableau d'exemple dans le PDF (page 16)

## Structure d'un agent

* 4 types de base (ordre de généralité grandissante):
    * Agent simple réflexe
    * Agent réflexe avec état interne
    * Agent basé sur les buts
    * Agent basé sur l'utilité
* Peuvent tous être transformés en agent apprenant

### Agent simple réflexe

* Choisit son action uniquement selon sa **perception courante**
* Ignore les perceptions précédentes
* Exemple aspirateur:

```python
    def reflex_vacuum_agent(location, status):
      if status == Dirty:
        return Suck
      elif location == A
        return Right
      elif location == B
        return Left
```

### Agent réflexe avec état interne

* Analyse comment le monde évolue au fil des actions
* Analyse l'impact de ses actions
* Change son état en fonction de ses observations courantes et passées

### Agent basé sur les buts

* Facile si action A mène directement au but
* Sinon, algorithme de recherche/planification
* Analyse actions passées et futures, recherche la meilleure possibilité selon l'état courant
* Plus flexible qu'un agent réflexe, car on peut **changer les buts sans réécrire toutes les règles**

### Agent basé sur l'utilité

* Buts ne font pas la distinction entre _heureux_ et _pas heureux_
* Fonction d'utilité: état -> nombre (valeur)
* Utile dans le cas où il y a plusieurs buts en conflit:
    * Vitesse et sécurité
* À quel point je vais être _heureux_ si je vais dans l'état B vs l'état C
* Comment serait le monde si je fais l'action A

## Agent apprenant

* Il est (presque) impossible de tout définir le comportement _a priori_
* On a recourt à l'apprentissage pour:
    * Simplifier la conception
    * Obtenir un agent plus flexible
    * Permettre à l'agent d'agir dans un environnement inconnu et devenir meilleur avec le temps

* Chaque action -> conséquence passe par le module d'apprentissage
* Rétroaction et génération de problèmes pour un meilleur apprentissage
* Application de changements au processus décisionnel en fonction des expériences passées

### Agent taxi apprenant

* Plusieurs modules forment l'agent

* Module de performance:
    * Connaissance et procédures pour choisir les actions
    * Taxi fait des actions sur la route: conduit n'importe comment
* Critique:
    * Observe l'agent et donne des infos au module d'appentissage
    * Observe le mécontentement des autres conducteurs et avertit le module d'apprentissage
* Module d'apprentissage:
    * Modifie le module de performances
    * Élabore une règle disant que c'est une mauvaise action
    * Ajoute cette règle dans le module de performance
* Générateur de problèmes:
    * Identifie les possibilités d'amélioration et suggère des expérimentations
    * Détecte un besoin d'amélioration
    * Suggère d'expérimenter d'autres façons de faire (conduite sur neige)

\pagebreak

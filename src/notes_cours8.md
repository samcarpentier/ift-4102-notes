# Planification

## Quelques définitions

* Littéral: expression atomique (littéral positif) et son opposé (littéral négatif)
* En logique propositionnelle, P est un atome (littéral positif) et $\neg$ P aussi (littéral négatif)

## Agent planificateur

* Construit des plans pour atteindre ses buts et les exécuter
* Similaire aux agents résolution problèmes (Chap. 3-4)
* Utilisation de représentation explicite = meilleure direction de la recherche par le processus de planification
* Environnement: complètement observable, déterministe, statique, discret
* Limite: quand la séquence d'action à accomplir est trop complexe:
    * Aller chercher du lait, des bananes, un perceuse
    * **Algos de recherche inadéquats: test de but et heuristiques ne peuvent pas faire l'affaire ici**

* Avantages:
    1. Ouverture de la représentation des actions
        * Sélection plus fine
        * États/buts: logique du premier ordre
        * Actions: description précondition-effet
        * Connexion directe entre les états et les actions
    2. Relâcher les contraintes pour la construction séquentielle d'actions
        * Planifier = ajouter actions quand elles sont requises
        * Réduit le facteur de branchement (on ne part pas nécessairement de l'état initial de façon incrémentale)
            * On peut acheter du lait sans savoir où en acheter
            * Pas de lien entre l'ordre de planif et d'exécution
    3. Approche diviser pour régner: division en sous-buts
        * Sous-plan pour chaque but
        * chercher lait ET chercher bananes ET chercher perceuse
        * Fonctionne tant que coût de réconciliation n'est pas trop grand

## Langage STRIPS

* Langage de représentation de base pour planificateurs

* Représentation des états:
    * Conjonction de littéraux positifs (Pos, À, At)
    * $Pos(avion1, Québec) \land Pos(avion2, Montréal)$
    * Littéraux ne contiennent pas de variables de fonctions: `Pos(x, y)` ou `Pos(Père(Simon), Montréal)` non permis!
    * **Tout ce qui n'est pas mentionné est supposé FAUX**
* Représentation des buts:
    * Conjonction de littéraux positifs
    * Un état satisfait un but ssi il contient TOUS les littéraux du but (ou plus)
    * État: riche ET populaire ET triste satisfait le but: riche ET populaire
* Représentation des actions:
    * Défini par préconditions et effets
    * Ajout de littéraux positifs et retrait de littéraux négatifs

* Préconditions: littéraux sans fonction qui spécifie ce qui doit être vrai avant d'exécuter l'action
* Effet: Littéraux représentant comment l'état change après l'action

### Fonctionnement

* État résultant **est identique** à l'état précédent avec:
    * Ajout des littéraux positifs de l'effet
    * Retrait des littéraux négatifs de l'effet
* Tous littéraux non-mentionnés ne changent rien
* Réviser exercice du robot Shaky pour examen (notation STRIPS)

### Heuristique de recherche

* Sous-buts indépendants
* Optimiste: si interactions négatives -> atteinte d'un but dans un sous-plan = pas obligé de l'atteindre dans un autre sous-plan
* Pessimiste: actions redontantes -> 2 actions identiques dans sous-plans peuvent être remplacées en une seule dans le plan final

### Simplifier le problème

* Enlever toutes les préconditions: toute action devient exécutable à tout moment
* Enlever les effets négatifs: on les néglige dans les sous-plans

### Espaces

* Noeuds = états
* Chemin = plan

* Recherche prog avant: état init -> état final. Grand facteur _b_
* Recherche prog arrière: état final -> état init. Limite le facteur _b_

* Noeuds = plans partiels
* Opérateurs s'appliquent sur des plans
* Chemin $\neq$ important
* Départ avec plan incomplet -> applique opérateur pour compléter plan et devenir consistant

* Opérateurs:
    * Ajouter un lien: Action vers précondition non résolue
    * Ajouter une étape: Résoudre précondition non résolue
    * Réordonner (étape p/r à une autre)

## Représentation des plans

* Engagement minimum: seulement sur aspects pertinent des actions
* Ordre partiel: seulement quelques actions du plan
* Ordre total: représentation de toutes les étapes (linéarisation du plan)

### Composantes d'un plan

* Ensemble d'actions
* Plan vide = [Start, Finish]
* Ensemble de contraintes d'ordonnancement
* Ensemble de lien causaux: intervalle de protection (aucune insertion entre les 2 états; force l'ordonnancement)
* Ensemble de préconditions non résolues: but de planificateur -> réduire à l'ensemble vide

### Solution

* Plan complet:
    * Tout préconditions satisfaites par des actions
    * Pas annulées par d'autres
* Plan consistant:
    * Pas de **contradiction** entre contraintes d'ordonnancement (liens causaux)
    * A < B, B < C, C < A
* On a toujours `Start` < `Finish`

## Algorithme POP (Partially ordered planification)

* Plan init:
    * Actions Start et Finish
    * Contrainte Start < Finish
    * Aucun lien causal
    * Précond non-résolues (PNR) = toutes celles de Finish
* Test de but: **Retourne plan solution si ensemble des préconditions non-résolues est vide**

* Fonction successeurs:
    * Choisir arbitrairement PNR p d'une action B
    * Générer tous plans consistants pour action A permettant d'accomplir p
    * Lien causal ApB, A < B ajoutés au plan
    * Si A est nouveau:
        * Start < A et A < Finish

* Résoudre conflits entre liens causaux (ajout de contraintes)

### Heuristique pour POP

* Doit choisir PNR et la satisfaire
* Peut y avoir valeur comme "variable moins contraignante" pour les CSP
    * **Précondition satisfaite avec le moins d'actions**

## Graphes de planification

* Structure de données permettant de formuler heuristiques efficaces
* Applicable à toutes méthodes de recherches vues à date
* Possibilité d'extraire le plan à partir du graphe

* Correspond à une séquence de niveaux <=> étapes dans le temps (niveau 0 = état init)
* Chaque niveau contient:
    * Tous les littéraux pouvant être vrais à un temps donné
    * Toutes les actions pouvant avoir leurs préconditions satisfaites à un temps donné
* **Fonctionnent uniquement pour des problèmes de planif en logique propositionnelle**

### Condition d'arrêt et liens mutuellement exclusifs

* On arrête la construction du graphe quand on obtient 2 niveaux identiques
* On obtient:
    * Chaque niveau Ai contient toutes les actions pouvant être exécutées en Si
    * Chaque niveau Si contient tous les littéraux d'une action A(i-1)
* Un littéral n'apparaissant pas dans le graphe ne peut être exécuté par aucun plan

* Liens mutex présents si:
    * Effets inconsistants: action rend faux un effet d'une autre
    * Interférence: effet d'une action est la négation d'une précond d'une autre
    * Compétition: précond d'une action est mutex avec la précond d'une autre action

### Algorithme GRAPHPLAN

* Voir algo et démo dans les notes (chap.10, p.51)

# CSP: Constraint satisfaction/solving problem

* Ensemble de variables: X1, X2, ..., Xn
* Ensemble de contraintes: C1, C2, ..., Cn

* État: affectation de valeurs pour quelques unes ou toutes les variables
* Affecation consistante: aucune violation de contraintes
* Affectation complète: toutes les variables ont une valeur
* **Solution: Affectation complète et consistante**
* Solution optimale: nécessité d'une fonction objective
* Voir exemple coloration provinces australiennes

## Types de CSP

### Variables discrètes

* Domaines finis (nb var n et taille max du domaine d)
    * Affectations complètes possibles en O(d^n)
* Domaines infinis (entiers, chaines de char, etc)
    * Ordonnancement de tâches, variables = jour début/fin
    * Demande un langage de contraintes (débutTache1 + 5 < débutTache3)
    * Contraintes linéaires (résolvables)
    * Contraintes non-linéaires (non décidables -> pas de réponse par oui/non au problème avec un algo)

### Varialbes continues

* Temps début et fin de chaque observation téléscope Hubble
* Cas particulier des _contraintes linéaires_:
    * doiventêtre les égalités ou des inévalités linéaires formant une région **convexe**
    * Forment un programme linéaire résolvable en un temps polynomial (p/r au nombre de variables)

### Types de contraintes

* Unaire: concernent une seule variable
* Binaire: concernent deux variables
* Ordre plus élevé: concernent 3+ variables
* Préférences (rouge est mieux que vert): problème représenté par un coût d'affectation

## Applications des CSP

* Assignations: qui enseigne quel cours (TP1)
* Problèmes d'horaire: quelle classe offerte ou et quand
* Planif transport/usine

## Formulation de recherche standard

* États: valeurs assignées jusqu'à présent
* État initial: vide {}
* Fonction successeurs: affecter valeur libre qui ne cause pas de conflit
* Test de but: affectation est-elle complète?

* Même formulation pour tous les CSP
* Toutes les solutions à n var apparaissent à la profondeur n
    * Algo depth-first (commutativité)
* Chemin n'est pas important: on peut utiliser algos itératifs comme récursifs

## Recherche à retour arrière (backtracking)

* Assignation de vars commutative
* Chaque noeud -> on considère seulement une var

### Améliorer l'efficacité de la recherche backtracking

* Quelle var assignée dans quel ordre?
* Quelles sont les implications des affectations de la var courante sur les autres non-affectées
* Quand chemin échoue, peut-on éviter cet échec?

### Ordre des variables

* Heuristique du nb min de valeurs restantes (MRV)
    * Choisir la var avec le moins de valeurs restantes possibles
    * Problème échoue plus rapidement: réduit l'arbre de recherche
* Heuristiue du degré
    * Choisir la var qui présente le plus de contraintes
    * Réduit le facteur de branchement _b_

### Ordre des valeurs

* Heuristique des valeurs moins contraignantes (LCV)
    * Choisir la valeur qui enlève le moins de choix pour les autres vars

#### Forward checking (vérification avant)

* Maintient toutes les valeurs possibles pour chaque var (MRV pour les vars)
* Défaut: propagation de contraintes renforce contraintes locales
* On développe étape par étape en faisant des choix arbitraires
* On arrête dès que l'on rencontre un cul-de-sac (0 valeur possible pour une variable)

#### Cohérence des arcs

* Arcs d'un graphe quand il y a une contrainte entre 2 "états" voisins
* Développer les valeurs possible après chaque décision selon les arcs sortant de l'état courant
* S'arrête dès que l'on rencontre un cul-de-sac 

* Un arc entre X et Y est cohérent si pour toute valeur x de X il y a au moins une valeur possible y de Y
* Si X perd une valeur, les voisins de X doivent être revérifiés
* Trouve erreurs plus vite que forward checking
* Peut être utilisé comme pré-processing ou après chaque assignation
* Algo AC-3 dans le livre et notes de cours

### Gestion de contraintes spécifiques

* Contrainte `AllDiff`, toutes différentes:
    * Chaue variable a une valeur distincte
    * Si _m_ vars, _n_ valeurs et _m > n_, alors contraintes pas satisfaites
    * Algo:
        * Elever vars qui n'ont qu'une valeur dans leur domaine et enlever ces valeurs des des autres vars
        * Continuer tant qu'il y a des vars avec un domaine ne contenant qu'une seule valeur
        * Si domaine vide se produit (ou qu'il y a plus de vars que de valeurs dsipo): **incohérence**

* Heuristique _min conflict_:
    * Fonctionne bien pour le problème des 8 reines
    * Fonctionne bien pour les algos "online" (1 action, regarde état)
    * CSP dans Hubble: calcul 1 semaine à 10 minutes en utilisant _min-conflict_

## Structure des problèmes

* Graphe des contraintes pour trouver solutions rapidement
* Composantes connexes = sous-problèmes indépendants
* On résout les sous-problèmes indépendamment
* Si chaque sous-problème a _c_ vars sur un total de _n_, alors coût en pire cas = `$n/c * d^c \in O(n)$`:
    * Avec n=80, d=2 et c=20 (calcul à 10M de noeuds par seconde): 
        * CSP complet = 2^80 -> 4 milliards d'années à résoudre
        * 4 * 2^20 = 0.4 secondes!!!

### Structure en arbe des poblèmes

* Ramener problème à un graphe
* Fixer valeur d'une variable et la retirer du graphe
* Possibilité de diviser en sous-problèmes
* Utilisation du **tri topologique**:
    * Algo: 
        * Choisir noeud facile et ordonner les autres (parent précède toujours)
        * Pour j dans [n..2], `EnleverIncohérence(Parent(Xj), Xj)`
        * Pour j dans [1..n], affecter valeur à `Xj` **consistante** avec `Parent(Xj)`

* Si structure proche d'un arbre, on peut fixer la valeur de certains noeuds pour obtenir un arbre
* Utilisation de la cohérence des arcs

### Décomposition en sous-problèmes

* Chaque var dois être dans au moins un sous-problème
* Chaque contrainte dans au moins un sous-problème
* Si une var apparait dans 2 sous-problèmes, elle doit être dans tous les sous-problèmes sur le chemin entre les 2
* Complexité: `O(n*d^y)` ou _d_ est la grandeur du sous-problème

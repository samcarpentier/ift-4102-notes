# Résolution de problèmes à l'aide d'algorithmes de recherche

## Agents de résolution de problèmes

1. Formuler un but: ensemble d'états à atteindre
2. Formuler un problème: états et actions à considérer
3. Recherche de solution: examiner les différentes séquences d'actions menant au but et choisir la meilleure
4. Exécution: accomplir la séquence d'actions

### Exemple du graphe entre les villes

* On est à A et on veut aller à D
    * But: atteindre D
    * Problème:
        * États: vertices du graphe
        * Actions: Aller d'un vertex à l'autre
    * Solution:
        * Une séquence de vertex
        * Ex.: A-C-E-D
    * Environnement très simple (statique, observable, discret, déterministe)

* Formulation du problème
    * État initial: l'état x auquel se trouve l'agent
    * Actions: fonction S(x) qui permet de trouver l'ensemble d'états successeurs à x (sous forme de paires [action, successeur])
    * Test de but: vérifie si le but est atteint
    * Coût du chemin: Directement lié à la performance de l'agent dans ce cas-ci

## Abstraction

* Il faut éliminer les détails pour faciliter le travail de l'agent
* On ne garde que le strict minimum, car environnement humain est **trop complexe**
* Abstraction valide ssi on a un mapping (solution abstraite : solution réelle) pour chaque solution abstraite
* **Sans abstraction, l'agent ne peut rien faire**

## Recherche de solution dans un arbre

* Simule l'exploration de l'espace d'états en générant les successeurs pour chaque état exploré
* Établissement des noeuds parents
* Sélection de l'action qui nous dirige vers le but
* Coût du chemin emprunté
* Profondeur (nombre d'étapes dans le chemin à partir de l'état initial)

## Stratégies de recherche

* But: déterminer dans quelle ordre il faut parcourir les noeuds
* Recherche non-informée:
    * Aucune info additionnelle
    * On ne sait pas si un noeud est meilleur qu'un autre
    * On sait seulement si le noeud est un but ou non
* Recherche informée (heuristique):
    * Peut estimer si un noeud est plus **prometteur** qu'un autre

## Évaluation des stratégies (**IMPORTANT**)

* Complétude: algorithme **garanti de trouver une solution** s'il en existe une
* Optimalité: stratégie trouve la **solution optimale**
* Complexité de temps: combien de **temps** est nécessaire pour trouver **la solution**
* Complexité d'espace: de quelle **quantité de mémoire** a-t-on besoin?

### Complexité

* _b_, le facteur de branchement: le nombre **maximum** de successeurs à un noeud
* _d_, la profondeur: profondeur du **noeud but** le **moins profond**
* _m_: la **longueur maximale** d'un chemin dans l'espace d'états

## Stratégies de recherche non-informées

* Largeur d'abord (breath-first)
* Coût uniforme (uniform-cost)
* Profondeur d'abord (depth-first)
* Profondeur limitée (depth-limited)
* Profondeur itératif (iterative deepening)
* Recherche bidirectionnelle (bidirectional search)

* Généralement peu efficaces
* Aveugles! On ne sait pas si on s'approche du but
* **Caractéristiques de chaque stratégie dans le PDF (pages 14 à 33)**

## Stratégies de recherche informées (heuristiques)

* Utilisent une fonction d'estimation (heuristique) pour choisir stratégiquement les noeuds à visiter

* Meilleur d'abord (best-first)
* Meilleur d'abord glouton (greedy best-first)
* A*
* Algorithmes à mémoire limitée
    * IDA* (iterative deepening A*)
    * RDFS (Recursive best-first search)
    * SMA* (simplified memory-bounded A*)

* **Caractéristiques de chaque stratégie dans le PDF (pages 36 à 56)**
* Heuristique admissible: ELLE NE SURESTIME PAS (si vraie distance = 100, h(n) <= 100, sinon pas admissible)
* Heuristique consistente: ELLE PRODUIT TOUJOURS MÊME RÉSULTAT POUR MÊME INPUT

## Inventer une heuristique

* Simplifier un problème
* La solution exacte du problème simplifié peut être utilisée comme heuristique
* Coût solution optimale pour problème simplifié <= coût solution optimale vrai problème
* **Voir exemples simples dans le livre**

### Génération automatique d'heuristiques

* Obtenue en testant plusieurs simplifications du problème
* Si plusieurs heuristiques et aucune dominante, utilisation d'une composite:
    * `h(n) = max{h1(n), h2(n), ..., hm(n)}`, la fonction la plus précise pour le noeud concerné

* Sous-problème:
    * 8-Puzzle: on trouve une solution pour les cases 1-2-3-4 sans se préoccuper des autres pièces

\pagebreak

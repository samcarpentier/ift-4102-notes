# Au-delà de l'exploration classique

* Escalade (hill-climbing)
* Recuit simulé (simulated annealing)
* Recherche locale en faisceau (local beam)
* Algorithmes génétiques

## Algorithmes d'amélioration itérative

* Dans la plupart des cas, le chemin pour arriver à la solution n'est pas important
* Espace de recherche = ensemble des **configurations complètes** et le but visé:
    * Trouver solution optimale
    * Trouver configuration qui satisfait **toutes** les contraintes du problème
* On part avec une **configuration complète** et, par amélioration **itérative**, on **améliore la qualité** de la solution
* Résolution dans des espaces d'états grands ou inifinis
* Utilise peu de mémoire

* Problème des _n_ reines:
    * Mettre _n_ reines sur une planche d'échecs _n_ x _n_
    * Aucune reine sur même ligne ni même colonne
    * Config initiale ne respecte pas
    * On bouge une reine à la fois jusqu'à ce que toutes les contraintes soient respectées

### Redémarrage aléatoire

* Si on arrête de progresser:
    * Redémarrer avec un nouveau point de départ
    * Backup de la meilleure solution à date
    * On va finir par tomber sur la bonne solution
* Dépendant de la _surface_ d'états:
    * Si assez smooth, on va trouver la solution optimale rapidement
    * Si beaucoup de maxima locaux, temps qui tend vers exp
* Généralement peu d'itérations = solution raisonnable

### Hill-climbing

* Choisir successeur ayant la meilleure évaluation p/r au noeud courant
* Arrête quand aucun successeur n'a une plus grande valeur
* Aucune mémorisation ni exploration en avant (**bad**)
* Peut jammer pour plusieurs raisons (maximum local, plateau, etc.)

### Simulated annealing

* Permettre des **déplacements perturbateurs** pour échapper aux maxima locaux
* Agiter les résultats pour faire sortir des maxima locaux, mais pas assez pour sortir du maxima global (analogie balle ping-pong)

* Boucle interne identique à Hill-climbing, sauf qu'elle choisit au hasard au lieu du meilleur
* Si amélioration, accepté
* Sinon, accepte avec déplacement P < 1:
    * P diminue (exp.) selon la mauvaise qualité de la solution (delta E)
    * P diminue à mesure que T diminue (mauvais déplacements autorisés quand T est élevé)

### Local beam

* Sélection de _k_ états aléatoires
* Générer tous les successeurs des _k_ états à chaque étape
* Si but trouvé, arrêtes
* Sinon, choisir les _k_ meilleurs successeurs et recommencer

* Recherche stochastique:
    * Fonction d'évaluation décide de la probabilité de sélection
    * Sélection aléatoire selon la fonction de probabilité pour les _k_ états

### Algos génétiques

* Principes de l'évolution naturelle:
    * Individu fort = plus de chances de survie
    * 2 individus forts = enfants forts
    * Mutations surviennent parfois (bénéfiques ou mortelles)
    * **Utilisation de ces principes pour générer hypothèses**

* Hypothèses = chaînes de bits, expressions symboliques ou programmes informatiques
* Repro + mutation = génération suivante
* Fonction d'utilité détermine chance de reproduction des hypothèses

* Représentation des hypothèses:
    * Chaînes de bits
    * Règles de type _si, alors_:
        * Ciel {ensoleillé, nuageux, pluvieux}:
            * 010: ciel = nuageux
            * 011: ciel = nuageux _OU_ pluvieux
    * Représentation contien toujours toutes les sous-chaînes, même si l'attribut de la sous-chaîne n'est pas dans la règle (longueur fixe)

* Opérateurs génétiques:
    * Croisement:
        * Générer 2 enfants en swappant 2 sections entre les parents
    * Mutation:
        * Inverser un bit au hasard
    * Peuvent être adaptés aux représentations

* Algorithme:

```{r eval=FALSE}
        Sélectionner situation initiale

        TANT QUE (NON condition d'arrêt):
            Sélectionner partie population selon leur adéquation
            Sélectionner meilleures paires de solutions
            Produire nouvelle génération (croisements et mutations)
```

* Paramétrage:
    * Nombre d'individus dans la population:
        * Trop peu = deviennent tous semblables
        * Trop grand = temps de calcul ingérable
    * Taux de mutation:
        * Trop peu = pas assez de nouveaux traits dans la population
        * Top grand = générations pas assez semblables
    * Taux de reproduction
    * Nombre maximum de générations
* Par **essai-erreur** généralement

#### Exemple

* Voyageur de commerce: trouver le chemin **le plus court** passant par toutes les villes **en revenant au point de départ**
    * Gêne: ville
    * Individu: liste de villes
    * Population: ensemble de parcours

## Recherche online (peu important)

* Tous les algos précédents sont dits "offline"
    * Calculent solution complète avant de l'exécuter

* Agent de recherche "online":
    * Fait une action
    * Observe l'environnement
    * Choisir sa prochaine action

* Algos assez différents: tenir compte des déplacements de l'agent
* Adapter Hill-climbing et DFS:
    * On ne mémorise que l'état local et on développe ses successeurs
* Learning real-time A* (LRTA*)
* Fonction `C(S)`: estimation du coût pour atteindre le but

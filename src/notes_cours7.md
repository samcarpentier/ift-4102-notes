# Agents logiques

* Connaissance -> Raisonnement -> Action dans le monde
* Quel est l'état du monde?
* Qu'est-ce qu'il faut accomplir?
* Quelles sont les conséquences des actions?

## Base de connaissances

* Ensemble de représentations de faits concernant le monde
    * Chaque représentation = énoncé
    * Exprimé dans un langage formel
* Agents logiques basés sur des connaissances
* Raisonnement (logique) portant sur les actions possibles dans le monde

### Structure

* KB:
    * Contient les énoncés
    * Contenu dépendant du domaine
    * Base de l'agent logique
* Moteur d'inférence:
    * Contient les algorithmes d'inférence
    * Contenu indépendant du domaine
    * S'appuie sur la KB pour faire ses déductions

### Gestion des connaissances

* Énoncés représentent le monde: Formulés et ajoutés dans la BC (base connaissance)
* Ajout de connaissance:
    * Action `tell`
    * Interrogation `ask`
* Aucune invention, on ne fait que des déductions à partir de la base de connaissances (moteur d'inférence)

### Capacités d'un agent logique

* Représenter les états/actions
* Incorporer nouvelles perceptions (percevoir)
* **Mettre à jour sa représentation du monde**
* **Déduire propriétés cachées du monde**
* **Déduire les actions appropriées**

### Description

* Niveau connaissances: l'agent de décrit par ses connaissances
    * Si saleté, alors nettoyer
* Niveau implémentation: Structures de données de la KB et des algos d'inférence

### Connaissances initiales

* Déclarative: connaissances initiales ajoutées avec `Tell` avant toutes les perceptions
* Procédurales: comportements désirés sont programmés directement
* Possibilité d'apprendre: devient un agent autonome

## Monde du Wumpus (PEAS)

* **P**erformance, **E**nvironnement, **A**ctuateurs, **S**enseurs
* Jeu monstre, or et trous (points +/- pour chaque action/conséquence)
    * Performance: +1000 pour or, -1000 tomber trou, -1 pour chaque action, -10 pour tirer flèche
    * Environnement: grille 4x4, agent commence en (1,1), localisations de l'or, du Wumpus (monstre) et des trous sont aléatoires
    * Actuateurs: Peut bouger, peut lancer flèche, peut ramasser l'or
    * Senseurs: Puanteur quand proche du Wumpus, brise quand case adjacente à un trou, scintillement pour l'or, Wumpum cri quand il meurt

### Propriétés de l'envirommenent du Wumpus

* Observable: Non, perception locale
* Déterministe: Oui, effet des actions sont spécifiés exactement
* Épisodique: Non, séquentiel
* Statique: Oui, Wumpus/trous ne bougent pas
* Discret: Oui
* Multiagent: Non, Wumpus est composante de l'enviromment, pas un agent

### Décisions difficiles

* Quand aucun déplacement n'est sécuritaire:
    * Lancer une flèche vers Wumpus
        * Si Wumpus n'était pas là, sécuritaire
        * Si Wumpus était là, sécuritaire aussi
        * Flèche consommée...

## La logique

* Logiques: langages formles pour représenter l'info: déduire des conclusions
* Syntaxes: Définit les configs possibles pouvant consituer les phrases
* Sémantique: Définit le sens d'un énoncé (phrase), définit la véracité
* Résultat V/F: limite de la logique

### Langage arithmétique

* Syntaxe: `x + 2 > y` est un énoncé, mais `x2 + y >` n'en est pas un (invalide)
* Sémantique:
    * `x + 2 > y` est vrai si `x + 2` est plus grand que `y`
    * `x + 2 > y` est vrai dans un _monde_ où x=7 et y=1
    * `x + 2 > y` est faux dans un _monde_ où x=0 et y=6

### Inférence

* La basse de connaissance (BC/KB) infère $\alpha$ si $\alpha$ est vrai dans **tous les mondes** où KB est vrai
* Notation: $KB |= \alpha$
* Exemple:
    * `x + y = 4` permet d'inférer que `4 = x + y`
* Relations cause-conséquence

#### Modèles

* _m_ est un modèle d'un énoncé $\alpha$ si $\alpha$ est vrai dans _m_
* $M(\alpha)$ est l'ensemble de tous les modèles de $\alpha$
    * $KB |= \alpha$ ssi $M(KB) \subseteq M(\alpha)$

#### Algorithmes

* Si un algo d'inférence _i_ permet d'inférer $\alpha$ à partir de KB:
    * $KB |-i \alpha$
* Un algo d'inférence préserve la véracité si:
    * $KB |-i \alpha => KB |= \alpha$
* Un algo d'inférence est complet si:
    * $KB |= \alpha => KB |-i \alpha$

### Remarques

* Fait vs énoncé:
    * Fait = partie intégrante du monde
    * Énoncé = encodé, puis manipulé par l'agent
    * Énoncé = représentations des faits
    * Mécanismes d'AI opèrent sur les énoncés et non les faits
* Processus d'inférence:
    * Soit on:
        1. Génène des énoncés à partir de la KB courante
        2. Vérifie si un énoncé peut être dérivé à partir de la KB courante
    * **Impossible d'inférer 2 énoncés qui se contredisent dans la KB**

## Ingénierie ontologique

* Ontologie (informatique): représentation des ensemble, des données, etc (tous les concepts abstraits) en informatique

* Quand problème complexe, bonne représentation nécessaire:
    * Plus générale
    * Plus flexible

### Catégories

* Actions -> niveau des objets
* Raisonnements -> niveau des catégories
* **LPO**: Langage du premier ordre

#### Caractéristiques

* Disjointes: aucun membre en commun
* Décomposition exhaustive: Déconstruire catégorie en tous les sous-ensembles possibles
    * Canadiens, Américains, Mexicains -> décomposition exhaustive de la catégorie _Nord-Américains_
* Partition: décomposition exhaustive et disjointe
    * Mâle, femelle -> partition de la catégorie _Animale_

#### Composition physique

* Quand un objet fait partie d'un autre: `PartOf`
    * `PartOf(Quebec, Canada)`
* Quand on a composition d'objets avec parties définies, mais sans structure: `BunchOf`
    * Ensemble abstrait
    * `BunchOf({Pomme1, Pomme2, Pomme3})`
    * Relation ci-dessus $\neq$ l'ensemble _Pommes_

### Prédicats

* Phrases catégoriques avec structure sujet/prédicat:
    * Platon est un homme
    * Le train siffle
    * Socrate est mortel
* Identifie une propriété (prédicat) à propos d'un individu (sujet)
* Phrase = application fonctionnelle d'un prédicat à un individu

* Avec LPO, utilisation de prédicats avec les objets pour représenter les catégories:
    * `BallonDeBasketball(b)` ou `BallonDeBasketballs`
    * `Membre(b, BallonDeBasketballs` ou $b \in BallonDeBasketballs$
    * `SousEnsemble(BallondeBasketballs, Ballons)` ou $BallonDeBasketballs \subset Ballons$

* L'héritage permet de simplifier les KB
    * Catégorie nourriture -> Toutes les instances de nourriture sont _comestibles_
    * Fruit $\subset$ Nourriture
    * Pomme $\subset$ Fruit
    * Alors, pomme est comestible
* Les relations entre les sous-classes organisent les catégories en **taxonomie**

### LPO et catégories

* Un objet est membre d'une catégorie: $BB_9 \in Basketballs$
* Une catégorie est une sous-classe d'un autre: $Basketballs \subset Balls$
* Tous les membres d'une catégorie ont certaines propriétés: $x \in Baskbetballs => Round(x)$
* Reconnaitre membres catégories via certaines propriétés:
    * $Orange(x) \land Round(c) \land Diameter(x) = 9.5 \land x \in Balls => x \in Basketballs$

## Mesures

* Représentation en fonction des unités
    * `Longueur(L) = Pouces(1.5) = Centimetres(3.81)` 
* Expression des conversions:
    * `Centimetres(2.54 * d) = Pouces(d)`
* Grandeurs qualitatives:
    * `Difficulté(Examen1) < Difficulté(Examen2)`

## Situation _Calculus_

* Actions: termes logiques
    * Tourner, avancer
* Situations:
    * Initiale (S0)
    * Toute autre situation résultant d'une action
        * Fonction `Resultat(a, s)` donne le résultat d'une action à un situation donnée
        * Toutes les situations sauf S0 sont e résultat d'une action
    * Parallèle avec le monde du _Wumpus_

* Fluents:
    * Fonction/prédicats qui varient d'une situation à l'autre
* Intemporel/éternet
    * Fonctions/prédicats qui ne varient pas d'une situation à l'autre

### Séquençage des actions

* Séquence vide ne change pas la situation: `Resultat([], s) = s`
* Exécuter séquence = exécuter le premier + le reste de la séquence:
    * `Resultat(a|seq, s) = Resultat(seq, Resultat(a, s))`

### Décrire les actions

* Utilisation d'axiomes
    * Possibilité: quand est-il possible d'exécuter l'action
        * `Preconditions => Poss(a, s)`
    * Effet: que se passe-t-il quand l'action est exécutée
        * `Poss(a, s) => Changements qui résultent de l'action`

#### Difficulté

* L'axiome d'effet représente _ce qui change_, mais pas _ce qui ne change pas_ (Frame problem)
* Comment gérer ce qui ne change pas?
    * Représentation: on ne peut pas lister tout ce qui ne change pas à chaque action
    * Inférence: on veut éviter de tout recopier à répétion pour avoir l'état

#### Axiome Successor-state

* Règle le problème de représentation "frame problem"
* Axiome:
    * Si l'action est possible:
        * Si `fluent` est vrai dans l'état résultant soit:
            * L'effet de l'action l'a rendu vrai
            * Il était vrai et l'action n'a rien changé

* Wumpus: $Poss(a, s) => (Holding(g, Result(a, s)) <=> a = Grab(g) \lor (Holding(g, s) \land a \neq Release(g)))$

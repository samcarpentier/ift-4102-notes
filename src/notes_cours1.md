# Introduction à l'AI

## Qu'est-ce que c'est?

* 4 points:
    * Penser comme des humains
    * Agir comme de humains
    * Penser rationnellement
    * Agir rationnellement

### Penser comme des humains

* Fonctionnement du cerveau humain
* Implémenter les théories et comparer avec les humains
* Validation des systèmes
    * Prédire et tester comportements de sujets humains
    * Valider à partir de données neurologiques
* Un interrogateur humain ne doit pas pouvoir faire la différence entre un répondant humain et un système d'AI

### Agir comme des humains

* Test de Turing:
    * Traitement langage naturel
    * Représenter connaissances
    * Raisonnement automatique
    * Apprentissage

### Penser rationnellement

* Logique (déduction, etc.)
* Logique formelle = énoncés sur les objets du monde et leurs interrelations
* Maths + Philo -> AI
* Problèmes:
    * Traduire connaissances en équations logiques
    * Logique est descriptive: limitée quand il faut faire rouler des applications complexes

### Agir rationnellement

* Faire la bonne chose selon les infos disponibles
* Agent rationnel: Entité qui perçoit et agit dans un environnement pour accomplir ses **buts** en fonction de ses capacités et connaissances
* **But du cours: conception d'agents rationnels**
* Maximiser performances pour une tâche donnée

## Préhistoire et histoire de l'AI

* Beaucoup de blabla...

\pagebreak
